<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="<?=base_url('lib/css/bootstrap.css')?>">
	

	<script src="<?=base_url('lib/js/jquery.min.js')?>"></script>
	<script src="<?=base_url('lib/js/bootstrap.min.js')?>"></script>
</head>
<style type="text/css">
	.head{color:#51211B;}
	.green{color:#AEE025;}
	.blue{color: #20948F;}
</style>
 <body>
  	<div class="container">
		<div class="row " ><br><br>
			<div class="col-lg-2"></div>
			<div class="col-lg-8 well">
				
					<h3 class="head">Contact Us | ASMAN</h3>
					<h4>New Contact has been requested from <?=$fullname?></h4>
					<hr>
					<h4 class="head">Full Name :<strong class="green"><?=$fullname?></strong></h4>
					<h4 class="head">Email :<strong class="green"><?=$email?></strong></h4>
					<h4 class="head">Phone :<strong class="green"><?=$phone?></strong></h4>
					<h4 class="head">Message :<strong class="green"><?=$message?></strong></h4>
					<hr>
				
				
			<div class="footer">
				<h4><STRONG class="blue">Thanks and regards,<br>Powered with 
						<i class="fas fa-heart"></i>  
						by	</STRONG><br>
					<!-- <b>
						<a href="https://sochware.com" target="_blank">
							<span style="color:#88c540;">Soch</span>
							<span style="color:#174587">Ware</span>
						</a>
					</b> -->
				</h4>
				<a href="https://sochware.com" target="_blank">
					<img src="<?=base_url('lib/images/sochware_logo.jpg')?>" alt="SochWare" width="150px" />
				</a>
			</div>
			</div>
			<div class="col-lg-2"></div>
		</div>
	</div>
 </body>
 </html>