<div id="message">
	<div id="fh5co-feature-product" class="fh5co-section-gray">
		<div class="" style="margin-bottom: -50px">
			<div class="row">
				<div class="col-md-12 text-center heading-section">
					<h3>Message from the President</h3>
					<blockquote class="blockquoteItalic">
						'Not all of us can do great things.  But we can do small things with great love.' - Mother Teresa
					</blockquote>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
				<!-- <div class="col-md-1 text-left animate-box"></div> -->
				<div class="col-md-5 text-center animate-box">
					<img src="<?=base_url('lib/images/teams/president.JPG')?>" alt="Messaage From President" width=100% height="auto">				
					<div class="text-right">
						<strong>- Pratistha Thapa</strong><br>
						<i>President</i>
						<p>Association of St. Mary's Alumnae Nepal - ASMAN</p>
					</div>
				</div>
				
				<div class="col-md-7 text-left animate-box">				
					<p>
						<strong>Dear ASMANites and our supporters,</strong><br><br>
						&nbsp;&nbsp;“For knowledge and virtue”. Having spent my formative years at St. Mary’s, our school motto has been the very ethos of my being. I can proudly say I am who I am thanks to the school, my teachers and my friends.<br>
						What started as a journey of little girl in starched uniforms running around the school, has come a full circle today. I am forever grateful to St. Mary’s for giving me the courage to be bold and take opportunities when they arose. I take this opportunity to thank our school for making me believe in myself and not letting me hold back for the fear of failure. Believe me, I have had my share of failures but the values instilled in me by the school and our teachers have taught me to march forward with the head held high. Thank you for teaching me how to be a leader and a follower as well. I look forward to being both in our new executive committee. <br>
						Members of our new committee is an amalgamation in youthful exuberance, enthusiasm and experience of the past. Our team shall focus on making lasting impact on lives of the underserved population. We are committed to ASMAN’s objective of contributing for better of children and women from marginalized communities. We look forward to joining hands with you all to make the next two years successful. <br>
						I would like to take this opportunity to thank the outgoing ASMAN Committee for two wonderful years. We started as complete strangers and have built a family around our team. ASMAN has grown a lot in past two years and we are confident that we will keep the momentum going on. <br>
						Message for our new team: There is no I in TEAM and there is no US without U. Coming together is a beginning, keeping together is a progress and working together is success. Thank you for joining to be part of this team to create Success! <br>
						Lastly, as someone rightly said, “Giving is not just about making a donation. It is about making a difference”. Join hands with ASMAN to make a difference in lives of those who need it the most. <br>
						Thank you !
					</p>
				</div>
				<!-- <div class="col-md-1 text-left animate-box"></div> -->
			</div>
			
		</div>
	</div>
</div>