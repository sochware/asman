<!DOCTYPE html>
<html>
<head>
	<title>ASMAN | HOME</title>	
	 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<?php include 'header/css_inc.php'; ?>
<body>
<div id="home">
	<div id="fh5co-wrapper">
		<div id="fh5co-page">
			<?php 
				$this->load->view('inc_view/messenger_api');
				$this->load->view('inc_view/floating');
				$this->load->view('inc_view/header_menu');
				$this->load->view('inc_view/flashdata');
				$this->load->view('view_home/slider');
				$this->load->view('view_home/feature');
				$this->load->view('view_home/message');
				$this->load->view('view_home/gallery');
				$this->load->view('view_home/philanthropist');
				//$this->load->view('view_home/project.php');
				$this->load->view('view_home/news');
				$this->load->view('inc_view/footer');
			 ?>
		</div>
	</div>
</div>
<div class="modal fade bd-example-modal-lg" id="asman-notice">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<img src="<?=base_url('lib/images/notice/asman_notice.jpg')?>"  width="100%">
			</div>
		</div>
	</div>
</div>
<?php include 'header/js_inc.php'; ?>
<script>
    $(document).ready(function(){
        $('#asman-notice').modal('show');
    })
</script>
</body>

</html>