<div id="team_profile"><br>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 text-center heading-section animate-box">
			<h3>Our Team</h3>
				<!-- <p>Take a closer look. We won't bite !</p> -->
		</div>
	</div>


	<div class="row row-bottom-padded-md">
		<div class="">
		    <div class="container">
		    	<div class="row">
		        	<?php foreach($team->result_array() as $t): ?>
		        	<div class="col-md-3 col-sm-6">
		        		<div class="our-team" id="<?=$t['sw_id']?>" onclick="location.href='<?=base_url('team/#popup')?>';" style="cursor: pointer;">
		        			<img src="<?=base_url('lib/images/teams/'.$t['sw_image'])?>" alt="<?=ucfirst($t['sw_name'])?>"/>
		                   
		                    <div class="team-content">
		                        <h3 class="team-prof">
		                            <a href="#"><?=ucfirst($t['sw_name'])?></a>
		                            <a href="#"><small><?=ucfirst($t['sw_desg'])?></small></a>
		                        </h3>
		                        <ul class="social-link">
		                            <li>
		                            	<a href="<?=$t['sw_fb']?>" target="_blank" class="mdi mdi-facebook"></a>
		                            </li>
		                            <li>
		                            	<a href="<?="mailto:".$t['sw_email']?>" target="_blank" class="mdi mdi-email"></a>
		                            </li>
		                        </ul>
		                    </div>
		                </div>
		            </div>

	            	

		            <?php endforeach;?>
		        </div>
			</div>
		
			
		</div>
	</div>
</div>

<!-- <div class="popup" id="popup">
    <div class="popup-inner" style="margin-top: 100px;">
	      <div class="popup__photo">
		        <img src="<?=base_url('lib/images/teams/'.$t['sw_image'])?>" alt="">
			       
	      </div>
	      <div class="popup__text">
	      		<h1><?=ucfirst($t['sw_name'])?></h1>
		        	<h2><?=ucfirst($t['sw_desg'])?></h2><br>
		        <p><?=($t['sw_about'])?></p>
	      </div>
	      <a class="popup__close" href="#">X</a>
    </div>
</div> -->
