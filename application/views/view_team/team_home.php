<!DOCTYPE html>
<html>
<head>
	<title>TEAM | ASMAN</title>
</head>
<?php include 'header/css_inc.php'; ?>

		
	<link rel="stylesheet" href="http://cdn.materialdesignicons.com/3.5.95/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="<?=base_url('lib/css/owl.carousel.css')?>">
	<link rel="stylesheet" href="<?=base_url('lib/css/team_style.css')?>">

<body>
<div id="home">
	<div id="fh5co-wrapper">
		<div id="fh5co-page">
			<?php 
				$this->load->view('inc_view/messenger_api');
				$this->load->view('inc_view/floating');
				$this->load->view('inc_view/header_menu');
				$this->load->view('inc_view/flashdata');
				$this->load->view('view_team/team');
				$this->load->view('inc_view/footer');
				// foreach($team->result_array() as $p)
				// {
				// 	echo "<pre>".print_r($p)."</pre>";
				// }
			 ?>
		</div>
	</div>
</div>
<?php include 'header/js_inc.php'; ?>
</body>

</html>