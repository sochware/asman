<br>
<div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="well well-sm">
                    <form class="form-horizontal" method="post">
                        <fieldset>
                            <legend class="text-center header">Contact us</legend>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <?=form_hidden('token',$token)?>
                                    <input id="fname" name="fname" type="text" placeholder="First Name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <input id="lname" name="lname" type="text" placeholder="Last Name" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <input id="email" name="email" type="email" placeholder="Email Address" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <input id="phone" name="phone" type="tel" placeholder="Phone" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <textarea class="form-control" id="message" name="message" placeholder="Enter your message for us here. We will get back to you within 2 business days." rows="7" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg" name="contactUs">Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div>
                    <div class="panel panel-default">
                        <div class="text-center header-map">Our Office</div>
                        <div class="panel-body text-center">
                            <h4>Address:</h4>
                            <div>
                            Kathmandu Residency <br/>
                             Lalitpur, Nepal, Ring Road,<br/>
                             Lalitpur 44600, Nepal<br />
                            +977-9843637716<br />
                            <a href="mailto:info@asman.net.np">info@asman.net.np</a><br/>
                            </div>
                            <hr />
                            <!-- <div id="map1" class="map">
                            </div> -->
                            <div id="map" class="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6B4H7XPGS15CSjzkc7BdptN_Ls0tsQeA&callback=initMap">
</script>
<script>
    function initMap()
    {
        var uluru = {lat: 27.660271, lng: 85.317245}; 
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h2 id="firstHeading" class="firstHeading">ASMAN</h2>'+
            '<div id="bodyContent">'+
            "<p><b>Association of St. Mary's Alumnae Nepal - ASMAN</b></br>" +
            'Thapathali Road, Kathmandu</br>'+
            '+977-01-4245726'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString;
        });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'Association of St. Mary\'s Alumnae Nepal - ASMAN'
        });
        infowindow.open(map, marker);
        marker.addListener('click', function() {
          infowindow.toggle(map, marker);
        });
    }
</script>
