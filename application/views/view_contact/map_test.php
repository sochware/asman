<!DOCTYPE html>
<html>
  <head>
    <style>
      #map {
        min-width: 300px;
        min-height: 300px;
        width: 100%;
        height: 100%;
       }
    </style>
  </head>
  <body>
    <h3>My Google Maps Demo</h3>
    <div id="map"></div>
    <script>
      function initMap()
    {
        var uluru = {lat: 27.691372, lng: 85.316769}; 
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h2 id="firstHeading" class="firstHeading">ASMAN</h2>'+
            '<div id="bodyContent">'+
            "<p><b>Association of St. Mary's Alumnae Nepal - ASMAN</b></br>" +
            'Thapathali Road, Kathmandu</br>'+
            '+977-01-4245726'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'Association of St. Mary\'s Alumnae Nepal - ASMAN'
        });
        infowindow.open(map, marker);
        marker.addListener('click', function() {
          infowindow.toggle(map, marker);
        });
    }
    // function initMap() {
    //   var uluru = {lat: 27.691372, lng: 85.316769}; 
    //   var map = new google.maps.Map(
    //       document.getElementById('map'), {zoom: 15, center: uluru});
    //   var marker = new google.maps.Marker({position: uluru, map: map});
    // }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6B4H7XPGS15CSjzkc7BdptN_Ls0tsQeA&callback=initMap">
    </script>
  </body>
</html>