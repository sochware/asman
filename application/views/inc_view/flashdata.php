<?php if($this->session->flashdata('success')): ?>
	<div class="alert alert-success alert-dismissible fade in" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	    <div align="center">
	        <?=$this->session->flashdata('success');?>
	    </div>
	</div>
<?php endif; ?>

<?php if($this->session->flashdata('error')): ?>
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	    </button>
	    <div align="center">
	        <?=$this->session->flashdata('error');?>
	    </div>
	</div>
<?php endif; ?>