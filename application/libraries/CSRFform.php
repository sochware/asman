<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class CSRFform {
	function __construct()
	{
		$this->ci=& get_instance();
	}

	function gentoken()
	{
		$gentoken=md5(uniqid(rand(),true));
		$this->ci->session->set_userdata('gentoken',$gentoken);
		return $gentoken;
	}
}