<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('CRUD');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}


	public function index()
	{
		if($this->input->post('token')==$this->session->userdata('gentoken'))
		{
			if (isset($_POST['contactUs'])) 
			{
				$contactUs['fullname']=$this->input->post('fname')." ".$this->input->post('lname');
				$contactUs['email']=$this->input->post('email');
				$contactUs['phone']=$this->input->post('phone');
				$contactUs['message']=$this->input->post('message');

				//$sql=$data['cat']=$this->CRUD->getNewsbyCatId();

				//$message="Your message is successfully sent.";
				$sql=$this->CRUD->contactUs($contactUs);
				if($sql)
				{

					$sender_email = 'webmaster@asman.net.np'; // Email 
					$user_password = 'YQzK*$Nm?+-L'; //Email password
					$replyto=$contactUs['email'];
					$receiver_email = 'link2rn@gmail.com';
					$username = 'ASMAN';
					$subject="Contact |ASMAN";
					
					// Configure email library
					$config['protocol'] = 'smtp';
					$config['smtp_host'] = 'ssl://mail.asman.net.np';
					$config['smtp_port'] = 465;
					$config['smtp_user'] = $sender_email;
					$config['smtp_pass'] = $user_password;
					$config['mailtype']  = 'html';
	            	$config['charset']  = 'iso-8859-1';

	            	// Load email library and passing configured values to email library
					$this->load->library('email', $config);
					$this->email->set_newline("\r\n");

					// Sender email address
					$this->email->from($sender_email, $username);
					// Receiver email address
					$this->email->to($receiver_email);
					// Subject of email
					$this->email->subject($subject);

					//reply to
					$this->email->reply_to($replyto);

					// Message in email
					$body = $this->load->view('view_email/contact',$contactUs,TRUE);
					// $body = implode(" ", $addMem);
					$this->email->message($body);	
					
						if($this->email->send())
						{
							$this->session->set_flashdata("success","Email Sent sucessfully !!");
							redirect('Contact');
							//$this->index();
						}
						else
						{
							$this->session->set_flashdata("error","Ops, Something thing went wrong !!");
							$this->index();
						}
				}
			}		
		}
		$data['cat']=$this->CRUD->getNewsbyCatId();
		$data['token']=$this->csrfform->gentoken();
		$this->load->view('view_contact/contact_dashboard',$data);
	}

	public function map()
	{
		$this->load->view('view_contact/map_test');
	}

}
